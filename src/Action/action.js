export const FilterByMonth = (month) => {
  return { type: "MONTH", payload: month };
};

export const FilterByCourse = (course) => {
  return { type: "COURSE", payload: course };
};
