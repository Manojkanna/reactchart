import { initialData, pdata } from "../Component/Constant";

let initialValue = {
  list: initialData,
  sortByMonth: "",
  sortByCourse: "",
};

export default function rootReducer(state = initialValue, action) {
  switch (action.type) {
    case "MONTH":
      let temp = [];
      if (action.payload) {
        pdata.forEach((value) => {
          if (action.payload === value.month && state.sortByCourse === "") {
            temp.push(value);
          }
          if (
            action.payload === value.month &&
            value.name === state.sortByCourse
          ) {
            temp.push(value);
          }
        });
        return { ...state, list: [...temp], sortByMonth: action.payload };
      } else {
        if (state.sortByCourse) {
          pdata.forEach((value) => {
            if (value.name === state.sortByCourse) {
              temp.push(value);
            }
          });
          return { ...state, list: [...temp], sortByMonth: "" };
        } else {
          return {
            ...state,
            list: [...initialData],
            sortByCourse: "",
            sortByMonth: "",
          };
        }
      }
    case "COURSE": {
      let temp = [];
      if (action.payload) {
        pdata.forEach((value) => {
          if (action.payload === value.name && state.sortByMonth === "") {
            temp.push(value);
          }
          if (
            action.payload === value.name &&
            value.month === state.sortByMonth
          ) {
            temp.push(value);
          }
        });
        return { ...state, list: [...temp], sortByCourse: action.payload };
      } else {
        if (state.sortByMonth) {
          pdata.forEach((value) => {
            if (value.month === state.sortByMonth) {
              temp.push(value);
            }
          });
          return { ...state, list: [...temp], sortByCourse: "" };
        } else {
          return {
            ...state,
            list: [...initialData],
            sortByCourse: "",
            sortByMonth: "",
          };
        }
      }
    }
    default:
      return {
        ...state,
        list: [...initialData],
        sortByCourse: "",
        sortByMonth: "",
      };
  }
}
