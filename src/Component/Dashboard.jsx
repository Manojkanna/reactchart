import { Courses, Months } from "./Constant";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  AreaChart,
  Area,
  BarChart,
  Bar,
} from "recharts";
import "./style.css";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FilterByCourse, FilterByMonth } from "../Action/action";
import { EmptyComponent } from "./EmptyComponent";
export const Dashboard = () => {
  const [card, setCard] = useState(true);
  const [line, setLine] = useState(true);
  const [area, setArea] = useState(true);
  const [bar, setBar] = useState(true);
  const data = useSelector((state) => state.data.list);
  const filterByMonth = useSelector((state) => state.data.sortByMonth);
  const filterByCourse = useSelector((state) => state.data.sortByCourse);
  const dispatch = useDispatch();

  const handleFilterByMonth = (month) => {
    dispatch(FilterByMonth(month));
  };

  const handleFilterByCourse = (course) => {
    dispatch(FilterByCourse(course));
  };
  return (
    <div className="mainContainer">
      <div className="header">
        <div className="title">Welcome to Dashboard</div>
      </div>
      <div className="filter">
        <div className="filterContent">
          <div>
            <label> Filter by Month : </label>
            <select
              name="month"
              value={filterByMonth}
              onChange={(e) => handleFilterByMonth(e.target.value)}
              className="filterSelect"
            >
              <option className="filterOption" value="">
                Please Choose...
              </option>
              {Months.map((month, index) => {
                return (
                  <option className="filterOption" key={index} value={month}>
                    {month}
                  </option>
                );
              })}
            </select>
          </div>
          <div>
            <label> Filter by Course : </label>
            <select
              name="course"
              value={filterByCourse}
              onChange={(e) => handleFilterByCourse(e.target.value)}
              className="filterSelect"
            >
              <option className="filterOption" value="">
                Please Choose...
              </option>
              {Courses.map((course, index) => {
                return (
                  <option className="filterOption" key={index} value={course}>
                    {course}
                  </option>
                );
              })}
            </select>
          </div>
          {(filterByCourse !== "" || filterByMonth !== "") && (
            <div>
              <button
                onClick={() => dispatch({ type: "CLEAR" })}
                className="clearFilter"
              >
                Clear Filter
              </button>
            </div>
          )}
        </div>
        <div className="filterChart">
          <div className="filterChartContent">
            <input
              type="checkbox"
              checked={card}
              onChange={() => setCard(!card)}
            />
            <label> Card</label>
          </div>
          <div className="filterChartContent">
            <input
              type="checkbox"
              checked={line}
              onChange={() => setLine(!line)}
            />
            <label> Line chart</label>
          </div>
          <div className="filterChartContent">
            <input
              type="checkbox"
              checked={area}
              onChange={() => setArea(!area)}
            />
            <label>Area Chart</label>
          </div>
          <div className="filterChartContent">
            <input
              type="checkbox"
              checked={bar}
              onChange={() => setBar(!bar)}
            />
            <label>Bar Chart</label>
          </div>
        </div>
      </div>
      {data.length !== 0 ? (
        <>
          {card && (
            <div className="cards">
              {data.map((value, index) => (
                <div key={index} className="subCards">
                  <div className="cardName">{value.name}</div>
                  {filterByMonth && <div>({filterByMonth} Course)</div>}
                  <div className="cardContent">
                    <div className="discount">Discount : {value.discount}%</div>
                    <div className="cardDetails">
                      <div className="cardSubDetails">
                        <span>Fees : </span>
                        {value.fees}K
                      </div>
                      <div className="cardSubDetails">
                        <span>No of Student : </span>
                        {value.student}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
          <div className="chart">
            {line && (
              <div className="subCharts">
                <h3 className="chartHeading">Student Fees Variance</h3>
                <ResponsiveContainer width={800} aspect={3}>
                  <LineChart
                    data={data}
                    margin={{ top: 5, right: 100, left: 20, bottom: 5 }}
                  >
                    <CartesianGrid strokeDasharray="3 3" stroke="white" />
                    <XAxis dataKey="name" stroke="white" />
                    <YAxis stroke="white" />
                    <Tooltip contentStyle={{ backgroundColor: "white" }} />
                    <Legend />
                    <Line
                      type="monotone"
                      dataKey="student"
                      stroke="#fbb532"
                      activeDot={{ r: 8 }}
                    />
                    <Line
                      type="monotone"
                      dataKey="fees"
                      stroke="#67f2cc"
                      activeDot={{ r: 8 }}
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>
            )}
            {area && (
              <div className="subCharts">
                <h3 className="chartHeading">Student Variance</h3>
                <ResponsiveContainer width={800} aspect={3}>
                  <AreaChart
                    data={data}
                    margin={{
                      top: 5,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" stroke="white" />
                    <XAxis dataKey="name" stroke="white" />
                    <YAxis stroke="white" />
                    <Tooltip />
                    <Legend />
                    <Area
                      type="monotone"
                      dataKey="student"
                      stroke="#fbb532"
                      fill="#fbb532"
                    />
                  </AreaChart>
                </ResponsiveContainer>
              </div>
            )}
            {bar && (
              <div className="subCharts">
                <h3 className="chartHeading">Fees Discount Variance</h3>
                <ResponsiveContainer width={800} aspect={3}>
                  <BarChart
                    data={data}
                    margin={{
                      top: 5,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" stroke="white" />
                    <XAxis dataKey="name" stroke="white" />
                    <YAxis stroke="white" />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="fees" fill="#fbb532" />
                    <Bar dataKey="discount" fill="#67f2cc" />
                  </BarChart>
                </ResponsiveContainer>
              </div>
            )}
          </div>
        </>
      ) : (
        <EmptyComponent />
      )}
    </div>
  );
};
